﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightScript : MonoBehaviour
{
    public Light lt;
    public bool night = false;
    public float dayNightTimer;
    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine("dayTimer");
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    

    public void dayChanger()
    {
        if(night == true)
        {
            lt.intensity = 0.2f;
        }
        else
        {
            lt.intensity = 1f;
        }
    }

    IEnumerator dayTimer()
    {      
        yield return new WaitForSeconds(dayNightTimer);
        if(night==true)
        {
            night = false;
        }
        else
        {
            night = true;
        }
        dayChanger();
        StartCoroutine("dayTimer");
    }
}
